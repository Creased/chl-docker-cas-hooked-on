#!/bin/bash

# import config
for ENV_FILE in $(find ./conf/ \( -name "cas-service.env" -o -name "docker*.env" -o -name "git*.env" \) -type f -print); do
	source ${ENV_FILE}
done

# trust git server
mkdir -p ${HOME}/.ssh/
touch ${HOME}/.ssh/known_hosts
echo "${SSH_SERVER_HOSTKEYS}" >${HOME}/.ssh/known_hosts

# dump ssh key
echo "${SSH_PRIVATE_KEY}" >${HOME}/.ssh/${GIT_SERVER}_private_key
chown ${USER}:${USER} ${HOME}/.ssh/${GIT_SERVER}_private_key
chmod 600 ${HOME}/.ssh/${GIT_SERVER}_private_key

# Run ssh-agent
# eval $(ssh-agent -s)

# clone cas project
echo -e "Host ${GIT_SERVER}\n  User git\n  IdentityFile ${HOME}/.ssh/${GIT_SERVER}_private_key" >${HOME}/.ssh/config
[ ! -d "${CLONE_PATH}" ] && git clone git@${GIT_SERVER}:${GIT_PATH}.git ${CLONE_PATH}

# get submodule
cd ${CLONE_PATH}
git fetch
git pull
git submodule update --init --recursive

# login to registry
docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASSWORD} ${REGISTRY}

# deploy service
docker stack deploy --with-registry-auth --compose-file docker-compose.yml ${SERVICE_NAME}
