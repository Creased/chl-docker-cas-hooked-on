#!/bin/bash

##
# Functions
#
spin() {
    ###
    # Variables
    #
    PID=${1:-''}
    INTERVAL=.08s
    SPINNER="⠋⠙⠚⠞⠖⠦⠴⠲⠳⠓"

    ###
    # Main process
    #
    if [ ! -z "${PID}" ]; then
        # printf "\033[?25l"
        while [ -d /proc/${PID} ]; do
            # printf "\033[2K\033[G[+] Requesting \033[0;33m${URL}\033[0m [ ]\033[1D"
            # printf "\033[D${SPINNER:i++%${#SPINNER}:1}"
            sleep ${INTERVAL}
        done
        # printf "\033[?25h\033[C\033[2K\033[G"
    fi
}

##
# Variables
#
OUT=$(mktemp)

exec 3>&1

# import config
source ./conf/cas-client.env

##
# Authentication test
#
URL="${SERVICE_URL}/login"

# Retrieve token
curl --silent --include --location --request "GET" \
     --header "User-Agent: ${CLIENT_UA}" \
     --url "${URL}" --output ${OUT} &

PID="${!}"

spin "${PID}"

## Display info
RESPONSE_CODE=$(cat ${OUT} | head -n1 | awk '{print $2}')
SIZE=$(cat ${OUT} | wc --bytes)

case "${RESPONSE_CODE}" in
    20[0-9])
        PROMPT=$'\033[1;32m[+]'
        STATUS="CAS OK"
    ;;
    30[0-9])
        PROMPT=$'\033[1;36m[*]'
        STATUS="Une redirection a eu lieu, merci de spécifier l'URL d'accès direct à CAS !"
    ;;
    40[0-9])
        PROMPT=$'\033[1;33m[?]'
        STATUS="CAS n'a pas été trouvé..."
    ;;
    * | 5[0-9])
        PROMPT=$'\033[1;31m[!]'
        STATUS="Une erreur est survenue pendant le chargement de CAS"
    ;;
esac

MESSAGE=$(printf "%s\033[0m %s\n" "${PROMPT}" "${STATUS}")
[[ ! "${RESPONSE_CODE}" -eq 200 ]] && echo "${MESSAGE}" >&2 || echo "${MESSAGE}"

# Submit authentication
if [[ "${RESPONSE_CODE}" -eq 200 ]]; then
    TOKEN=$(cat ${OUT} | python -c "
import sys;
from bs4 import BeautifulSoup as bs;

html = bs(sys.stdin, 'html.parser');
print html.find('input', {'name': 'execution'})['value'];
")

    curl --silent --include --location --request "POST" \
         --header "User-Agent: ${CLIENT_UA}" \
         --header "Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3" \
         --data "username=${CLIENT_USER}&password=${CLIENT_PASSWORD}&execution=${TOKEN}&_eventId=submit&geolocation" \
         --url "${URL}" --output ${OUT} &

    PID="${!}"

    spin "${PID}"

    ## Display info
    RESPONSE_CODE=$(cat ${OUT} | head -n3 | tail -n1 | awk '{print $2}')

    case "${RESPONSE_CODE}" in
        200)
            PROMPT=$'\033[1;32m[+]'
            STATUS=$(cat ${OUT} | python -c "
import sys;
from bs4 import BeautifulSoup as bs;
reload(sys);
sys.setdefaultencoding('utf-8');

html = bs(sys.stdin, 'html.parser');
print '{title}'.format(title=html.find('h2').getText());
")
        ;;
        *)
            PROMPT=$'\033[1;31m[!]'
            STATUS="An error occured"
        ;;
    esac


    MESSAGE=$(printf "%s\033[0m %s\n" "${PROMPT}" "${STATUS}")
    [[ ! "${RESPONSE_CODE}" -eq 200 ]] && echo "${MESSAGE}" >&2 || echo "${MESSAGE}"
fi

##
# End
#
# Clean temp file
rm ${OUT}

