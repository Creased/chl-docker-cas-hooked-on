Hooked-on
=========

Simple WEB hooks triggering API.

## Introduction ##

As part of a professional project to implement a CI/CD infrastructure based on [GitLab-CE](https://gitlab.com/gitlab-org/gitlab-ce), [GitLab-CI](https://docs.gitlab.com/ce/ci/), [Docker Swarm](https://docs.docker.com/engine/swarm/) and [Docker Compose](https://docs.docker.com/compose/), in order to include the Docker Swarm to the pipeline, I wanted to develop a simple interface allowing GitLab-CI to send a signal (dubbed WEB hooks) to the Docker Swarm manager to update the service based on the new image.

Hooked-on listens to these WEB hooks and runs process based given specified and implemented hook.

## Example ##

Create a `config.json` file (if you're not familiar with JSON, maybe you can try [JSON Editor Online](http://www.jsoneditoronline.org/?id=8384d530332b8f514eae7123fc210292):

```json
{
    "app": {
        "host": "0.0.0.0",
        "port": 8080,
        "env": "production"
    },
    "tokens": {
        "public": [
            "hello"
        ],
        "ee0874170b7f6f32b8c2ac9573c428d35b575270a66b757c2c0185d2bd09718d": [
            "demo"
        ]
    },
    "hooks": {
        "hello": {
            "type": "cmd",
            "payload": "printf \"Hello, World!\""
        },
        "demo": {
            "type": "script",
            "payload": "demo.sh"
        }
    }
}
```

Based on this configuration, we can establish the following hook table:

| URL      | Execute                  | Description    | Access            |
|----------|--------------------------|----------------|-------------------|
| `/hello` | `printf "Hello, World!"` | System command | Public            |
| `/demo`  | `demo.sh`                | Bash script    | Private to ee0874 |

See also: [API documentation](https://swaggerhub.com/apis/Creased/hooked-on/0.0.1-a).

## Demo ##

Start server:

```bash
npm install
npm start

```

Example:

![server-demo][]

Request for `demo` hook with token:

```bash
DATA=$(curl --silent -w "\n%{http_code}" --request "GET" \
     --url "localhost:8080/demo?token=ee0874170b7f6f32b8c2ac9573c428d35b575270a66b757c2c0185d2bd09718d")

```

Format data for fun:

```bash
CODE=$(echo "${DATA}" | tail -n-1)
DATA=$(echo "${DATA}" | head -n-1)

case "${CODE}" in
    20[0-9])
        PROMPT=$'\033[1;32m[+]'
    ;;
    30[0-9])
        PROMPT=$'\033[1;36m[*]'
    ;;
    40[0-9])
        PROMPT=$'\033[1;33m[?]'
    ;;
    * | 5[0-9])
        PROMPT=$'\033[1;31m[!]'
    ;;
esac
export PROMPT
echo "${DATA}" | python -c "
import sys, json, os;
data = json.load(sys.stdin);
print '{prompt}\033[0m {status}\n\n{output}'.format(prompt=os.environ['PROMPT'], status=data['status'], output=data['output'])"

```

Example:

![client-demo][]


Voilà!

<!-- Illustrations -->

 [server-demo]: illustrations/server_demo.png
 [client-demo]: illustrations/client_demo.png
