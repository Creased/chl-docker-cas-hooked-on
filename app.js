'use strict';

/*!
 * Author: Baptiste MOINE <contact@bmoine.fr>
 * Project: Hooked-on
 * Homepage: http://hook.bmoine.fr/
 * Released: 15/05/2017
 * Documentation: https://app.swaggerhub.com/apis/Creased/hooked-on/0.0.1-a
 */

// Modules
var app         = require('express')(),
    server      = require('http').createServer(app),
    os          = require('os'),
    childproc   = require('child_process'),
    fs          = require('fs'),
    path        = require('path'),
    logger      = require('morgan');

// Application configuration
var config = require('./config.json'),
    default_config = {
        'app': {  // Listening host and port
            'host': process.env.APP_HOST || '0.0.0.0',
            'port': process.env.APP_PORT || 8080,
            'env':  process.env.NODE_ENV || 'development'
        }
    };

config.app.host = config.app.host || default_config.app.host,
config.app.port = config.app.port || default_config.app.port
config.app.env  = config.app.env  || default_config.app.env,

// Logging
app.use(logger('combined', {
    stream: fs.createWriteStream(path.join(__dirname, '/logs/access.log'), {flags: 'a'})    // Create a write stream (in append mode)
}));
(config.app.env !== 'development') ? app.use(logger('combined')) : app.use(logger('dev'));  // Console stream

// Index
app.get('/', function (req, res) {
    res.render('index.ejs');
});

// API
app.get('/:hook', function (req, res, next) {
    var hook = config.hooks[req.params.hook],   // Get hook from URL
        priv = config.tokens[req.query.token],  // Get private hooks based on token from GET query params
        pub = config.tokens['public'],          // Get public accessible hooks
        out = {                                 // Initialize output to send to client
            'status': '',
            'output': ''
        },
        code = 0,                               // Default HTTP response code
        proc = {};                              // Process data

    if (hook !== undefined) {  // Check if hook is implemented in config.json
        if (pub.includes(req.params.hook) || (priv !== undefined && priv.includes(req.params.hook))) {  // Check if hook is part of public/private hooks
            switch (hook.type) {  // Run process payload based on hook configuration and type
                case 'script':
                    proc = childproc.exec('bash ./scripts/' + hook.payload);  // Run bash script in `scripts` folder
                    break;
                case 'cmd':
                    proc = childproc.exec(hook.payload);                      // Run system command
                    break;
            }

            proc.stdout.on('data', function (data) {  // Getting data from stdout (standard), define "HTTP/200 Ok" as potential response + fill-in the output
                code = ( (code !== 0) ? code : 200 );
                out.status = 'The hook was successfully executed';
                out.output += data;
            });

            proc.stderr.on('data', function (data) {  // Getting data from stderr (error), define "HTTP/500 Internal Server Error" as potential response + fill-in the output
                code = 500;
                out.status = 'An error occured while triggering hook';
                out.output += data;
            });

            proc.on('close', function (code) {  // Send data from process output with HTTP/500 higher priority
                res.writeHead(( (code !== 0) ? code : 200 ), {
                    'Content-Type': 'application/json'
                });
                res.end(JSON.stringify(out));
            });
        } else {  // Hook is not accessible to the specified token (public/private)
            out.status = 'Access denied to the specified hook';
            res.writeHead(403, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify(out));
        }
    } else {  // Hook is not implemented, return "HTTP/501 Not Implemented"
        out.status = 'Specified hook is not implemented';
        res.writeHead(501, {
            'Content-Type': 'application/json'
        });
        res.end(JSON.stringify(out));
    }
});

// Start server
server.listen(config.app.port, config.app.host, function () {
    console.log("Server is listening on %s:%d", config.app.host, config.app.port)
});
